package com.company.convert.shoes;

import com.company.convert.shoes.enums.ShoesName;

public class Shoes {

    ShoesName shoesName;
    double rate;

    public Shoes(ShoesName shoesName, double rate) {
        this.shoesName = shoesName;
        this.rate = rate;
    }

    public ShoesName getInformationName() {
        return shoesName;
    }

    public double getRate() {
        return rate;
    }


}