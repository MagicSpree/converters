package com.company.convert.shoes.enums;

import com.company.utils.Unit;

public enum ShoesName implements Unit {
    SM("Сантиметры"),
    RUS("Россия"),
    EU("Европа"),
    USA("Сша");

    private final String name;

    public String getName() {
        return name;
    }

    ShoesName(String name) {
        this.name = name;
    }
}