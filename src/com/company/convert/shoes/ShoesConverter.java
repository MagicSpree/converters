package com.company.convert.shoes;

import com.company.convert.shoes.enums.ShoesName;
import com.company.utils.Converter;
import com.company.utils.ConverterWithMap;

import java.util.HashMap;

public class ShoesConverter extends ConverterWithMap<ShoesName> {

    public ShoesConverter(ShoesName sourceShoes, ShoesName targetShoes) {
        super(sourceShoes,targetShoes);
        addShoes();
    }

    private void addShoes() {
        addUnit(ShoesName.SM, 25.0);// первая колонка значений по таблице
        addUnit(ShoesName.RUS, 39.0);
        addUnit(ShoesName.EU, 40.0);
        addUnit(ShoesName.USA, 7.0);
    }

    @Override
    public double convert(double sourceAmount) {
        ShoesName sourceShoes = getSource();
        ShoesName targetShoes = getTarget();
        double source = getUnitRate(sourceShoes);
        double target = getUnitRate(targetShoes);
        return sourceAmount-(source - target);  //считаем разницу между форматами стран и
    }                                           // отнимаем/прибавляем(если отрицательно) от искомого значения
}