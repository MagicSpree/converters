package com.company.utils;

import java.util.HashMap;

public abstract class ConverterWithMap<T extends Unit> extends Converter<T> implements Convert {

    private final HashMap<T, Double> units = new HashMap<>();
    protected ConverterWithMap(T source, T target) {
        super(source, target);
    }

    public void addUnit(T enumElement, double rate) {
        units.put(enumElement, rate);
    }
    /**
     * Функция позволяется получить значение из Map по ключу
     *
     * @param unitsName Объект "Ключ"
     * @return значение Enum объекта
     */
    public double getUnitRate(T unitsName) {
        return units.get(unitsName);
    }
}
