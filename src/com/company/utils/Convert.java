package com.company.utils;

public interface Convert {
      double convert(double sourceAmount);
}
