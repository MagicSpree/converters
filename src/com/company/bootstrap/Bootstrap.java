package com.company.bootstrap;

import com.company.convert.currency.CurrencyConverter;
import com.company.convert.currency.enums.CurrencyName;
import com.company.convert.information.InformationConverter;
import com.company.convert.information.enums.InformationName;
import com.company.convert.shoes.ShoesConverter;
import com.company.convert.shoes.enums.ShoesName;
import com.company.convert.temperature.TemperatureConverter;
import com.company.convert.temperature.enums.TempeatureName;
import com.company.utils.Converter;
import com.company.utils.Unit;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Bootstrap {

    Scanner scanner;
    Converter<?> converter;
    int sourceAmount;

    public Bootstrap() {
    }

    public void run() {
        scanner = new Scanner(System.in);
        int value = choosingAConverter();
        converterCreation(value);
        enterSourceAmount();
        double result = converter.convert(sourceAmount);
        printResult(result);
    }

    /**
     * Функция выбора конвертера.
     *
     * @return индекс выбранного конвертера
     */
    private int choosingAConverter() {
        print("Выберите конвертер: \n1.Валюта \n2.Единицы измерения информации \n3.Температура \n4.Размер обуви");
        print("Введите номер пункта: ");
        int value = getUserValue() - 1;
        while (checkRange(value, 0, 4)) {
            value = getUserValue();
        }
        return value;
    }

    /**
     * Функция создает выбранный пользователем конвертер
     *
     * @param point Пункт меню выбора соответствующего конвертера
     */
    private void converterCreation(int point) {
        switch (point) {
            case 0:
                CurrencyName sourceCurrency = getSourceOrTargetValue(CurrencyName.values(), "Выберите из какой валюты хотите перевести: ");
                CurrencyName targetCurrency = getSourceOrTargetValue(CurrencyName.values(), "Выберите в какую валюту хотите перевести: ");
                converter = new CurrencyConverter(sourceCurrency, targetCurrency);
                break;
            case 1:
                InformationName sourceInformation = getSourceOrTargetValue(InformationName.values(), "Выберите из какой единицы измерения информации хотите перевести: ");
                InformationName targetInformation = getSourceOrTargetValue(InformationName.values(), "Выберите в какую единицу измерения информации хотите перевести: ");
                converter = new InformationConverter(sourceInformation, targetInformation);
                break;
            case 2:
                TempeatureName sourceTemperature = getSourceOrTargetValue(TempeatureName.values(), "Выберите из какой единицы измерения температуры хотите перевести: ");
                TempeatureName targetTemperature = getSourceOrTargetValue(TempeatureName.values(), "Выберите в какую единицу измерения температуры хотите перевести: ");
                converter = new TemperatureConverter(sourceTemperature, targetTemperature);
                break;
            case 3:
                ShoesName sourceShoes = getSourceOrTargetValue(ShoesName.values(), "Выберите из какой системы измерения обуви хотите перевести: ");
                ShoesName targetShoes = getSourceOrTargetValue(ShoesName.values(), "Выберите в какую систему измерения обуви хотите перевести: ");
                converter = new ShoesConverter(sourceShoes, targetShoes);
                break;
        }
    }

    /**
     * Функция выводит меню выбора единиц измерения соответствующего конвертера
     *
     * @param enumObject Массив объектов Enum класса
     * @param message    Сообщение, выводимое пользователю
     */
    private <T extends Unit> void printUnits(T[] enumObject, String message) {
        StringBuilder stringBuilder = new StringBuilder(message);
        for (int i = 0; i < enumObject.length; i++) {
            stringBuilder.append("\n")
                    .append(i + 1)
                    .append(".")
                    .append(enumObject[i].getName());
        }
        print(stringBuilder.toString());
    }

    /**
     * Функция проверки диапазона введенного пользователем значения.
     *
     * @param value Значеничение введенное пользователем
     * @param min   Минимальное значение
     * @param max   Максимальное значение
     * @return boolean
     */
    private boolean checkRange(int value, int min, int max) {
        boolean check = true;
        if (!(value >= min && value <= max)) {
            print(String.format("Необходимо ввести число от %d до %d!", min + 1, max));
            check = false;
        }
        return !check;
    }

    /**
     * Функция возвращает Enum объект по выбранному пользователем индексу.
     *
     * @param element Массив объектов Enum класса
     * @param message Сообщение выдаваемое пользователю
     * @return объект Enum класса
     */
    private <T extends Unit> T getSourceOrTargetValue(T[] element, String message) {
        printUnits(element, message);
        print("Введите номер пункта: ");
        int indexSourceCurrency = getUserValue() - 1;
        while (checkRange(indexSourceCurrency, 0, element.length)) {
            indexSourceCurrency = getUserValue() - 1;
        }
        return element[indexSourceCurrency];
    }

    /**
     * Функция сканирует ввод пользователя
     *
     * @return значение пользователя
     */
    private int getUserValue() {
        try {
            return scanner.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Значение должно быть числом!");
            scanner.next();
            return getUserValue();
        }
    }

    /**
     * Функция ввода конвертируемого значения.
     */
    private void enterSourceAmount() {
        print("Введите значение: ");
        sourceAmount = getUserValue();
        if (!(converter instanceof TemperatureConverter)) {
            sourceAmount = Math.abs(sourceAmount);
        }
    }

    /**
     * Функция формирует строку с результатом и выводит ее.
     *
     * @param result Результат конвертации
     */
    private void printResult(double result) {
        String source = converter.getSource().getName();
        String target = converter.getTarget().getName();
        String resultToString = String.format("%.2f", result);
        print(String.format("%d %s конвертировано в %s %s", sourceAmount, source, resultToString, target));
    }

    private void print(String message) {
        System.out.println(message);
    }
}
