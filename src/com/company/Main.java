package com.company;

import com.company.bootstrap.Bootstrap;

public class Main {

    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.run();
    }
}
